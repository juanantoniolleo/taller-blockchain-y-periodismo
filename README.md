# TALLER BLOCKCHAIN Y PERIODISMO

TALLER BLOCKCHAIN Y PERIODISMO
Jornadas de Periodismo de Datos 2019, Medialab Prado, Madrid


BLOCKCHAIN Y PERIODISMO:

* LIBRO DE BLOCKCHAIN: https://libroblockchain.com/
	* COMUNIDAD BLOCKCHAIN (GRATUITO)
	* EL LIBRO DE SATOSHI (GRATUITO)
	* BLOCKCHAIN REVOLUCION (DE PAGO, FISICO Y DIGITAL)
		
* MEDIOS: Artículo de A. Peukschat / https://libroblockchain.com/medios/ 
- Incluye entrevista a  Covadonga Fernández, autora del capítulo  “Medios de Comunicación y blockchain”. 
- Por las características propias de Blockchain, que permite eliminar la confianza y por tanto desintermediar los procesos comerciales, “Blockchain puede ayudar a la profesión porque hay una cuestión muy importante subyacente, la monetización de los contenidos”, al igual que sucede con canciones, películas o videojuegos “porque esta tecnología permite realizar transacciones en céntimos de euro algo que hasta ahora era imposible y se abren otras posibilidades como pagar por contenidos concretos, suscribirse por días, contratar alertas de noticias, reportajes o artículos”. 
- En el ámbito de los medios de comunicación al uso, quedará aún más difuminado su papel y propiciará nuevos modelos de negocio más colaborativos y descentralizados. 
- “Por ejemplo se podrán crear plataformas tipo Uber en la que periodistas y usuarios conecten directamente unos con otros renumerándose por el contenido”.  
- Una situación posible que pude llevarnos a lo que califica de “vértigo del periodismo sin medios”, una expresión que recoge una situación “que no ha sucedido hasta ahora pero que las redes sociales nos han ido preparando” porque resulta evidente que, “a diario son muchas las personas que se acercan a Facebook google o twitter para informase y también los propios periodistas acuden a ellos y los utilizan como canal de comunicación para poder informarse y para informar”. 
- En este nuevo escenario, periodistas y lectores mantendrán un contacto directo, sin necesidad de intermediarios y podrán gestionar los contenidos directamente a través de contratos inteligentes “y decidir cómo se consumen esos contenidos” de forma flexible y soberana. 
- Pero no sólo eso, también es importante “para cuestiones relacionadas con la libertad de prensa”, y cita en el video un ejemplo real expuesto por el presidente de los periodistas europeos por la libertad de prensa en el Congreso Internacional Blockchain y periodismo organizado por eléchain. Un ejemplo extensible en aquellos “países donde los periodistas no pueden desarrollar libremente su profesión”.
- Sin embargo, con Blockchain se evitaría esta censura “porque conservas el anonimato y continúas siendo remunerado sin menoscabo de la información”
- En esta próximo revolución tecnológica-informativa que se nos viene encima, “el más beneficiado va a ser el consumidor como ha salido beneficiado de internet porque tendrá la libertad de suscribirse a lo que quiera, ver la publicidad que quiera y además ser pagado por ello”. Un nuevo escenario que le habilitará para “ser parte de la noticia aportando sus propios contenidos y poder convertirse en el ciudadano Kane del siglo XXI”. 
- Esta nueva situación bautizada por Fernández como de “los medios infinitos” abrirá la posibilidad de “contratar periodistas en todo el mundo, abriendo el abanico de publicación a cualquier persona, en cualquier lugar, en cualquier momento”.

* IX INFORME PERSPECTIVAS WELLCOMM DE LA COMUNICACIÓN:

	La irrupción del blockchain en el modelo de negocio de la comunicación y el periodismo plantea un nuevo escenario. ¿Podrán las agencias de comunicación sobrevivir si el cliente y sus públicos se comunican sin intermediarios? Eso es lo que intenta resolver el IX Informe Perspectivas wellcomm de la comunicación que se acaba de presentar.
	
	¿CÓMO CAMBIARÁ EL BLOCKCHAIN EL PERIODISMO?:
	El estudio recoge la visión de 20 expertos sobre los desafíos y oportunidades que pueden ofrecer al sector de la comunicación nuevos sistemas basados en la permanencia, la descentralización y la desintermediación.
	Y una de las conclusiones principales es que la irrupción de blockchain en el sector va a abrir la puerta a nuevos modelos de negocio. De hecho, apuntan, el periodismo ya cuenta con plataformas blockchain que conectan directamente a lectores con periodistas, y en las que se producen transacciones de contenidos por criptomonedas.
	
	Entre las novedades:
	* Surgirán plataformas en las que los periodistas cobren de sus lectores por investigar determinados aspectos que no cubren los medios convencionales.
	* Se crearán nuevos productos que las agencias de comunicación pueden ofrecer a sus clientes.
	* Y aparecerá una verdad permanente que dará garantías a la sociedad frente a noticias web que pueden editarse o desaparecer
	
De esta manera vemos cómo el blockchain, además de replantear el modelo de negocio del periodismo, también se abre como fuente de garantía frente a las noticias falsas o fake news. De hecho, los expertos que han participado en la investigación aseguran que puede ser una harramienta útil para ofrecer información de calidad y luchar contra la piratería o la desinformación.
	
Este es el primer informe sobre el sector registrado en una plataforma blockchain en España. Los archivos que lo integran han sido certificados en blockchain en la plataforma Nodalblock, de manera que es el primer estudio del sector publicado con marcas de tiempo, hash y número de nodos que lo validan, en un formato permanente y verificado mediante cadena de bloques.
En la presentación, Silvia Albert, Fundadora y Directora de Silvia Albert in Company y de wellcomm, ha reconocido que esta es la edición de Perspectivas "en la que más tenemos por aprender, dado que la revolución que viene está aún en ciernes, aunque los aspectos tecnológicos son los que están más definidos".
	
Por su parte, Rosa Matías, Directora de Proyectos en wellcomm y Directora de Comunicación Digital de Silvia Albert in Company, ha asegurado: “Los periodistas usaremos blockchain del mismo modo que llevamos décadas usando el correo electrónico: sin entender los entresijos técnicos de su funcionamiento”.

NOTAS INFORME PERSPECTIVAS WELLCOM:

* Internet 3.0, p2p, criptografía distribuida, Internet del Valor (la nueva internet)

* Si el frenesí de finales de los 90 (Internet 1.0) impulsó la infraestructura de banda ancha que posibilitó la Internet 2.0 -redes sociales, internet móvil-, las bases de datos distribuidas que registran públicamente el histórico de modificaciones (blockchain), posibilitarán servicios fidedignos sin necesidad de autoridades que sancionen su legitimidad, pues el registro de todas las entradas reside en una copia incremental que comparten todos los participantes. Nicolas Boullosa

* La eficiencia de blockchain permitirá micropagos de un céntimo por leer un artículo, y podrá ser el propio periodista quien lo cobre sin que intervenga su medio. Buenísima herramienta contra la piratería: blockchain conseguirá que el autor cobre por su trabajo cada vez que se consuma. Habrá un gran quiosco (Publicism, o Publiq, son dos ejemplos de futuras plataformas donde publicar contenidos periodísticos) en el que cada medio o cada autor cobren la cantidad exacta en función del tiempo que un lector pase leyéndolos. Habrá mejor periodismo: se terminará el fraude del clickbait (noticias-cebo) porque los medios no buscarán el clic, sino que pasemos tiempo en su página (cobrarán de anunciantes o lectores por tiempo de visualización y no por número de clics). Pablo Herreros
	
* ...la tokenización permite disfrutar del producto o servicio y, al mismo tiempo, facilita la distribución y el reparto del beneficio (o valor) entre todos los miembros, incluidos los usuarios. Estamos ante una tecnología que va a optimizar el sistema actual y que, además, es el germen de una nueva economía global -tokenomics o economía de la aportación, como quiera que la llamemos- que transformará el mundo de la comunicación. Beatriz Lizarraga
	
* El cambio que traerá blockchain a los diarios no descentralizará la línea editorial, no puede hacerlo. En cambio, pluralizará a sus trabajadores en una suerte de mercado y pondrá en evidencia las noticias falsas con registros inmutables de las publicaciones y sus cambios, que a la larga las eliminará. El periódico del futuro no contará con una plantilla fija de periodistas, editores y correctores, más bien tendrá un mercado interno de ‘sucesos a cubrir’, ‘trabajos a editar’ y ‘publicaciones por verificar’ que serán tomadas por periodistas disponibles al momento, previamente calificados y cuya reputación se moverá basada en la calidad de su trabajo (todo esto enmarcado en un contrato inteligente a cargo de algún representante de la línea editorial del periódico); y un mercado externo, en el que los periodistas pondrán voluntariamente en venta contenidos de su autoría que serán subastados en el tiempo para ser adquiridos por el medio que más los desee. Al final, el esquema no será muy distinto al desarrollo peer-reviewed de Bitcoin: cualquiera puede trabajar en ello, pero solo lo pertinente de alta calidad será integrado al protocolo. Héctor Cárdenas
	
* Comunicar en los tiempos de blockchain. ...Pero en la nueva economía de la atención, en cierta forma vertebrada alrededor de blockchain, necesita de los medios y las agencias de comunicación como, por ejemplo, la descentralización de infinitos mensajes requiere de la clasificación y ordenación del sentido de cómo interpretar el mundo que hay en un periódico. Puentes entre el presente de los medios de comunicación y su futuro, para lo que he creado BlockchainMedia.es Incluye también unos consejos para profesionales de comunicación blockchain. Covadonga Fernández

ARTICULO SOBRE LA PRESENTACION DEL INFORME:
	 * El blockchain obliga a repensar el modelo de negocio del periodismo:
	https://www.reasonwhy.es/actualidad/sector/el-blockchain-obliga-repensar-el-modelo-de-negocio-del-periodismo-2018-01-25

		
ARTICULOS SOBRE PERIODISMO Y BLOCKCHAIN:

* ¿BLOCKCHAIN PARA UN PERIODISMO COLABORATIVO Y DESCENTRALIZADO? - Covadonga Fernández
	 Blockchain..., nos anuncia el journalismtech. Un periodismo que nos hará dueños de los medios y nos traerá el periodismo colaborativo y descentralizado.
    En este nuevo modelo, las empresas tradicionales de medios deberán aprender a convivir con las jóvenes compañías tecnológicas que están irrumpiendo en su modelo de negocio. 
    Es lo que está sucediendo ya en otras industrias centenarias, como banca, telefonía, seguros, energía o el sector de la agricultura.
	https://retina.elpais.com/retina/2017/06/14/tendencias/1497438132_314400.html
	
* BLOCKCHAIN, REVOLUCIONA EL MUNDO Y EL PERIODISMO. - El Blog de Gemma Alcalá
		* Incluye una pequeña introducción al blockchain, qué es lo que ésta tecnología puede aportar al periodismo y cómo serán estos cambios para los propios periodistas.
	https://gemmaalcala.com/periodismo-tecnologia-blockchain/
	
* POR QUÉ LOS PERIODISTAS DEBERÍAN PREOCUPARSE POR EL BLOCKCHAIN:

	https://www.periodismo.com/2019/02/12/por-que-los-periodistas-deberian-preocuparse-por-el-blockchain/
	Daniel Sieberg, CEO de iO, director de iO Ventures, y cofundador de la red comunitaria de periodismo Civil (https://civil.co/), dice que:
    el blockchain, en pocas palabras, es simplemente otra forma de usar Internet.
	Una de las nuevas startups de tecnología, Proof, está intentando crear marcas de tiempo que ayudarán a los usuarios a verificar de dónde proviene el contenido, quién y dónde lo creó y subió, y ver su historial.
    De esta manera, las audiencias no tienen que depender de un periodista al considerar si una información es auténtica, podrán verificarse a sí mismas.
	Otra startup, Pressland, usa blockchain para ayudar a aumentar la transparencia en el sector del periodismo. 
    Rastrea y registra todo el recorrido de una pieza de contenido, desde el periodista hasta el que comprueba los hechos, hasta el editor y el medio.
    Pressland espera que al aumentar la transparencia sobre cómo se obtiene, edita y dispersa la información, aumentará la confianza del público en las marcas de medios.
	Otra ventaja de blockchain es que ofrece la posibilidad de recompensar a los usuarios con incentivos monetarios, generalmente en forma de criptomoneda. 
    Por ejemplo Publiq, la  plataforma potenciada por blockchain para la publicación de contenido, ofrece a sus usuarios recompensas en forma de tokens PBQ para producir o difundir contenido.
	ieberg afirma que una de las mayores ventajas de la tecnología es que cuando se crea un blockchain, siempre se puede agregar otro bloque, y construir una cadena que te ayude a rastrear parte de los datos hasta su fuente original.
    Si un editor necesita hacer una corrección, en lugar de solo sobrescribir el contenido original, permitirá que se corrijan y vean las correcciones al contenido original.
	Sieberg admitió que el objetivo de blockchain no es hacer que la publicación sea perfecta; la tecnología resolverá algunos problemas, y seguramente creará otros.
    Pero el blockchain puede ayudarnos a abordar algunos de los problemas más grandes con los que las organizaciones de noticias se enfrentan hoy, como la información errónea, la falta de responsabilidad, y la dificultad para establecer la autoría.
	
* EL VALOR DE BLOCKCHAIN EN PERIODISMO RESIDE EN DEVOLVER A PERIODISTAS Y ESCRITORES LA LIBERTAD ARTÍSTICA - Jarrod Dicker:

	https://www.blockchainmedia.es/single-post/2018/07/11/Jarrod-Dicker-el-valor-de-blockchain-en-periodismo-reside-en-devolver-a-periodistas-y-escritores-la-libertad-art%C3%ADstica
	Jarrod Dicker es el responsable de Po.et, la próxima plataforma para medios y creadores.
    Al menos, así es como se refiere a ella este especialista en literatura inglesa y amante de la escritura.
    Para Jarrod, el verdadero valor que blockchain trae al periodismo reside en su capacidad de devolver a periodistas y creadores la libertad artística que perdieron cuando empezaron a crear contenidos en función de cómo los usuarios comenzaron a buscarlos. 
    Po.et busca solucionar este problema creando reputación en la web. 
    
	* PO.ET:
	https://www.po.et/

OTROS TEMAS DE INTERES:		

* REMUNERACIÓN DE TRABAJOS:

* CONTRATOS INTELIGENTES:

* INTERCAMBIO DE VALOR:

* CAMPAÑAS DE MICROMECENAZGO:

* COMUNICACIONES ANÓNIMAS

* ORGANIZACIONES AUTÓNOMAS DESCENTRALIZADAS (DAO)



CASOS DE USO DE INTERES PARA PERIODISMO Y MEDIOS DE COMUNICACION:

* STEEMIT: Steemit es una plataforma que combina redes sociales y la protección de la propiedad intelectual. La plataforma basada en blockchain permite a las personas publicar contenido por el cual pueden ser recompensados en criptomoneda si a otros usuarios les gusta y promocionan su contenido.
    Esto garantiza que los creadores de contenido que publican artículos, videos e imágenes reciban una recompensa adecuada en relación con la popularidad de su contenido. Gestión de Derechos Digitales (DRM) Mediante smart contracts, se pueden automatizar los pagos de royalties en función del consumo que se haga sobre un producto digital. Esto impacta en la actual gestión de DRM de la industria de la música o el vídeo y puede hacer que los datos sean más transparentes, accesibles y fáciles de entender. (David Lastra, citado de informe wellcomm)
		https://steemit.com/
		
* CIVIL: La idea central es (relativamente) sencilla: una plataforma abierta de noticias que utiliza la tecnología blockchain y la criptoeconomía para permitir a ciudadanos y periodistas crear un espacio libre, seguro, descentralizado y sostenible en el que elaborar y compartir periodismo profesional.
    “En el mercado de CIVIL”, explica la plataforma en el Whitepaper que les sirve de manifiesto, “la audiencia puede apoyar directamente a los periodistas, y estos gestionan directamente sus espacios de noticias, o Newsrooms, de manera colaborativa”.
    El diseño de la plataforma se basa en los criterios de la criptoeconomía: a través de smart contracts, contratos automatizados apoyados en el entorno Ethereum y otras tecnologías blockchain relacionadas, se definen las premisas de todo el contenido y las interacciones de la plataforma basadas en cuatro actividades interrelacionadas. 
    La esencia de CIVIL es profundamente colaborativa: “Para construir Civil de manera adecuada, debemos hacerlo unidos”, proclaman. 
    Es por ello que invitan a todo aquel interesado a participar, con un enfoque constructivo, en el debate abierto en Github sobre la plataforma:
    (https://github.com/joincivil/whitepaper). The Civil Cryptoeconomic Whitepaper.
	
	* CIVIL Y CIVIL FOUNDATION:
	https://civil.co/
	https://civilfound.org/
	https://youtu.be/EEA9n802f1I

    The Civil Foundation supports journalism on a global level, with a primary focus on technology solutions.
    We are working in support of Civil, a community owned network dedicated to creating, sharing and supporting ethical journalism. 
    Civil is not meant to completely replace existing business models, but rather to enhance them. 
    The Civil network is made possible by blockchain technology which enables the network to run according a set of rules that the community controls, governed solely by clearly defined ethical journalism standards, known as the Civil Constitution.
    The Civil Foundation is partnering with institutions that hold a similar vision of supporting journalism. It takes a global village.
	
	* ARTICULOS SOBRE CIVIL: 
	* ‘Blockchain’ y periodismo, o cómo reinventar los medios (y ganar dinero)- Por Esther Paniagua

	https://retina.elpais.com/retina/2019/03/01/innovacion/1551430113_768462.html

	* Blockchain en el periodismo: Civil, “un mercado para el periodismo sostenible”:

	https://criptodinero.es/blockchain/blockchain-en-el-periodismo-civil/

* PUBLIQ: La Fundación PUBLIQ es un entorno de distribución de medios propiedad de una comunidad global independiente, gobernado y operado por esa comunidad cuyos miembros disfrutan del ilimitado potencial de la libre expresión y de la protección total de la identidad y de los derechos de propiedad intelectual. 
    PUBLIQ utiliza tecnologías de blockchain, inteligencia artificial y analítica para implementar un sistema de confianza universal entre sus miembros independientes. 
    El objetivo de la Fundación es dejar atrás el flujo masivo de noticias de baja calidad, falsas y prefabricadas, ya que su plataforma ofrece un proceso de recompensa transparente que incentiva que los miembros escriban contenido de calidad y la publicidad llegue a una audiencia específica de la forma más eficiente posible.
    La audiencia de PUBLIQ, a través de sus comentarios y acciones, da forma a la reputación de los autores; es lo que se denomina Calificación PUBLIQ. Esta calificación es la que se utiliza para calcular la reputación en tiempo real de los autores y su participación en las ganancias. Una vez que el artículo es publicado, el contenido se sella criptográficamente, se distribuye y es inmutable, lo que significa que ningún tercero puede alterar el trabajo de los autores. (Alexandre Tabbakh, citado de informe wellcomm)
	* PUBLIQ: We bring together content producers and publishers to forge a new path by directly creating, owning, governing and managing the media they create while enjoying instant and merit-based distribution of generated wealth.
	https://publiq.site/

		* ECOSISTEMA:

	https://publiq.network/en/ecosystem/
		Covadonga Fernández
		
* TOKENCONOMÍA: Nace tokenconomía, el periódico económico donde los periodistas son remunerados con tokens:

	https://testnet.publiq.site/s/5c55d4190686da757cb8b981	

* PUBLICISM: is passionate about free press.

	We think it is essential to an open and democratic society. Our blockchain based technology facilitates free press, by enabling citizens and journalists to speak freely and securely without fear of repercussion.
	Three years ago, blockchain was only discussed by a select group of hard-core IT aficionados. Today, almost $1 billion has been invested in blockchain start-ups. But despite the interest and intense work, no one has an end game in sight. There’s no killer app or key business proposition on the horizon yet. Financial institutions and fintechs recognise the potential of blockchain technology. But how will – or should – it be applied to other sectors? And better still, how will it support a better society?
	Soon after founding the social enterprise Publicism we noticed academic experts donors, and sponsors are willing to support our goals. So we started off in september 2016 as an international research project, researching:
	- the possibilities of blockchain supporting a decentralized news medium.
	- the anti-censorship possibilities of blockchain technology.
	– the possibilities of anonymity and pseudonymity.
	- the possibilities of safe monetization for journalists.

	The next phase – after our research – will be the creation of a dashboard that will make it easy for journalists to entrance and use a safe blockchain environment – in order to publish their content and data attached to that blockchain environment. After AB-testing based on the prototype. We aim to launch this dashboard first quarter of 2017.
	https://www.publicism.nl/
	
* POPULA:
	Popula es una publicación digital internacional de noticias y cultura alternativa en Civil, una nueva plataforma de publicación basada en tecnología Ethereum que se lanzará a principios de 2018. Como la publicación vive en redes informáticas distribuidas, ofrece a los lectores periodismo y arte sin anuncios, de origen y producción profesional, archivado de manera incorruptible y protegido contra la manipulación o la censura procedente de intereses gubernamentales o corporativos. Su intención no es sólo informar, sino también demostrar de forma transparente lo que un modelo distribuido sin anuncios puede hacer por los periodistas de todo el mundo. (María Bustillos, citado de informe wellcomm)

    Popula is a journalist-owned, journalist-run, ad-free publication on the Ethereum-based Civil publishing platform.
	Admite donaciones de ETH (Etereum), mediante Metamask, y también mediante sus propios tokens, que se pueden conseguir gratis (2$ al mes) si se deja una dirección de Metamask:
	Help: What is ETH?. ETH is Ether, a popular cryptocurrency generated on the Ethereum blockchain. 
	You’ll need some Ethereum cryptocurrency (ETH) in a MetaMask wallet in order to tip an author. Currently it’s not possible to tip in other cryptocurrencies, or in dollars or other fiat currencies. For a comprehensive FAQ to help get you started, please visit our help page, “How to Tip Your Favorite Authors with Cryptocurrency on Popula!”

	https://popula.com/

    * En este enlace se incluye una FAQ sobre criptomonedas y los micropagos con Metamask.

	https://popula.com/how-to-tip/

* BLOCKCHAIN OBSERVATORY, POR OléChain:

	OléChain promotes the Blockchain Observatory with the aim of generating a favorable environment for the development of this technology and its applications in companies, public services and people's lives.
	The Blockchain Observatory moves thanks to the power of the decentralized networks among all the actors that make up the ecosystem of this technology and is based on the capacity of the Communication, either disseminating all initiatives that add value to the Blockchain ecosystem.
	Calle de la Alameda 15, 28014. Madrid
	ursula@olechain.com / covadongaf@olechain.com
	Tel: (34) 619636958 | www.olechain.com
	https://www.olechain.com/

* PRESSLAND: Otra startup, Pressland, usa blockchain para ayudar a aumentar la transparencia en el sector del periodismo. Rastrea y registra todo el recorrido de una pieza de contenido, desde el periodista hasta el que comprueba los hechos, hasta el editor y el medio. Pressland espera que al aumentar la transparencia sobre cómo se obtiene, edita y dispersa la información, aumentará la confianza del público en las marcas de medios. Media trust through radical transparency. Pressland is the first comprehensive map of the global media supply chain, built to restore public trust.
	
    https://www.pressland.com

	
ENLACES A LAS INICIATIVAS MENCIONADAS EN EL ARTICULO DE MEDIOS, DEL LIBRO DE BLOCKCHAIN:

* STEEMIT: (VER ARRIBA)
	https://steemit.com/
			
* DECENT:
	https://decent.ch/
	- DCore: Launched in 2017, DCore is the blockchain you can easily build on. As the world’s first blockchain designed for digital content, media and entertainment, DCore provides user-friendly software development kits (SDKs) that empower developers and businesses to build decentralized applications for real-world use cases. 
	- Buy and sell digital content such as files, audio, video and other formats
	- Instant monetization, where content creators can set the price per view or download and get paid directly with no fees
	- Split revenue automatically for content with more than 1 contributor
	- Crowdfunding can turn your supporters into your advocates
	- DCT: is our very own altcoin. It is fundamental to our DCore blockchain which combines DPoS and file-sharing protocols. DCT is needed to fuel transactions and it also funds the witnesses that keep DCore running.
	Incluye su propia wallet y se puede adquirir de varias maneras.
	Desarrollado en JAVA.
			
* BREAKER, ANTES SINGULARDTV:
	- Welcome to Breaker. Formerly SingularDTV, our mission remains the same: to evolve the entertainment industry with a decentralized ecosystem that empowers creators and delivers fans a diverse collection of classic and original content.
	- Breaker is a blockchain-powered entertainment platform built with the artist in mind. The Breaker Artist Portal is your way to interact with the Breaker platform by uploading your content and controlling how that content is monetized. We welcome artists of all kinds to create their own channels to distribute their content globally. By bringing your content to Breaker, you will discover, through the power of blockchain, how you can create your own economy by doing what you love. 

    https://singulardtv.com/
    https://www.breaker.io/
			
* MONEGRAPH: We offer a public platform allowing anyone to register creative works on the Bitcoin blockchain along with ways to trade, buy and sell those works.

	https://monegraph.com/
			
* BRAVE: Navegador web

	https://brave.com/
			
* HYPER SPACE (ANTES SYNEREO): A New Revenue Stream for Creators. HyperSpace provides its users with a Universal Basic Income, just so they can support you with it. Our users are also financially incentivized to support you by sharing the content you post with new appreciating audiences.

	https://site.hyperspace.app/
	http://www.synereo.com/
	

IDENTIDAD DIGITAL:
	
* NODALBLOCK:
	https://www.nodalblock.com/es/

	* IDENTIDAD DIGITAL
	* CERTIFICACION DE ARCHIVOS
	* Nodalblock Connect: API REST, permite integrar los servicios de Nodalblock en cualquier aplicación empresarial.
	* NODALTICKET
	* NODALCHECK
	
* IDENTIDAD DIGITAL Y ‘BLOCKCHAIN’: COMO LLAVE AL CAMBIO DEL MUNDO:
	https://retina.elpais.com/retina/2017/06/05/tendencias/1496646930_763686.html
	
* SIMPOSIO BLOCKCHAIN: IDENTIDAD DIGITAL Y EL REGLAMENTO GENERAL DE PROTECCIÓN DE DATOS. MODELOS PRÁCTICOS Y CASOS DE USO:
	https://blockchainintelligence.es/event/conferencia-identidad-digital-en-blockchain/
	
* NODALBLOCK FUSIONA SU IDENTIDAD DIGITAL BLOCKCHAIN CON FIRMA ELECTRÓNICA CUALIFICADA:
	https://www.blockchaineconomia.es/nodalblock-firma-electronica/
	
* EL DÍA QUE 'BLOCKCHAIN' DEVOLVIÓ LA IDENTIDAD Y LA ECONOMÍA A LOS REFUGIADOS:
	https://www.technologyreview.es/s/9421/el-dia-que-blockchain-devolvio-la-identidad-y-la-economia-los-refugiados
	
* BLOCKCHAIN: EL CAMINO HACIA LA IDENTIDAD DIGITAL:
	https://www.ambito.com/blockchain-el-camino-la-identidad-digital-n5004782
	
ATRIBUCIÓN DE AUTORÍA DE TEXTOS E IMÁGENES:

* SIGNEBLOCK (DE PAGO, ALASTRIA): Certificamos la información a través de tecnología Blockchain
	https://www.signeblock.com/
	
* VERIBLOCK:
	https://www.veriblock.com/


VARIOS RECURSOS:

* Bit2Me Academy:
https://t.co/3JQ8VkMy81

* BLOCKCHAIN: A CHEAT SHEET:
https://www.techrepublic.com/article/blockchain-cheat-sheet/

* 17 Blockchain Applications That Are Transforming Society:
https://blockgeeks.com/guides/blockchain-applications/
